﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortCsharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // TODO Auto-generated method stub
            string[] sample = new string[8];
            char[] letter = new char[8];
            string[] num = new string[] { "", "", "", "", "", "", "", "" };
            int[] tStore = new int[8];
            char temp = ' ';
            int cnt = 0;
            int start = 0;
            int end = 0;
            string flist = "";
            List<string> s = new List<string>();
            s.Add("c10");
            s.Add("a10");
            s.Add("a1");
            s.Add("a2");
            s.Add("b2");
            s.Add("b100");
            s.Add("b3");
            s.Add("b99");
            s.Sort();

            for (int i = 0; i < s.Count; i++)
            {
                sample[i] = s[i].ToString();
            }

            for (int j = 0; j < sample.Length; j++)
            {
                for (int k = 0; k < sample[j].Length; k++)
                {

                    if (char.IsLetter(sample[j][k]))
                    {
                        letter[j] = sample[j][k];

                    }
                    if (char.IsDigit(sample[j][k]))
                    {
                        num[j] += sample[j][k];
                    }
                }
            }

            for (int p = 0; p < s.Count; p++)
            {

                if (p == 0)
                {
                    tStore[p] = Convert.ToInt32(num[p]);
                    start = 0;
                    temp = letter[p];
                }

                if (p > 0)
                {

                    if (temp == letter[p])
                    {
                        tStore[p] = Convert.ToInt32(num[p]);
                    }
                    else if (temp != letter[p])
                    {
                        end = p;
                        temp = letter[p];
                        tStore[p] = Convert.ToInt32(num[p]);
                        for (int i = start; i < end; i++)
                        {
                            for (int j = i + 1; j < end; j++)
                            {
                                int x;
                                if (tStore[i] > tStore[j])
                                {
                                    x = tStore[i];
                                    tStore[i] = tStore[j];
                                    tStore[j] = x;
                                }
                            }
                        }
                        start = end;
                    }
                }

            }

            for (int k = 0; k < tStore.Length; k++)
            {
                flist += letter[k];
                flist += tStore[k];
                sample[k] = flist;
                Console.WriteLine(sample[k]);
                flist = "";
            }
            Console.ReadKey();
        }
    }
}
